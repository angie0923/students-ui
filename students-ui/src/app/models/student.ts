export interface Student {
    id: number;
    firstName: string;
    lastName: string;
    studentID: string;
    email: string;
    gradeLevel: number;
    dateOfBirth: string;
    emergencyContactNumber: string;
}