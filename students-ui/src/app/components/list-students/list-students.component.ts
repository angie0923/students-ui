import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/models/student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.css']
})
export class ListStudentsComponent implements OnInit {

  // property
  students: Student[] = [];

  // dependency injection to bring in our service
  constructor(private studentSvc: StudentService) { }

  ngOnInit(): void {

    // call the list student
    this.listStudents();
  }

  // method that will display the list of our students / data
  // we have to subscribe to our observable method
  listStudents() {
    this.studentSvc.getStudents().subscribe(
      data => this.students = data
    )
  }

  // method that will delete the data using the deleteStudent() from our service
  deleteStudent(id: number) {
    this.studentSvc.deleteStudent(id).subscribe(
      data => this.listStudents()
    )
  }

}
