import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from 'src/app/models/student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  // properties
  newStudent: Student = {
    id: 0,
    firstName: '',
    lastName: '',
    studentID: '',
    email: '',
    gradeLevel: 0,
    dateOfBirth: '',
    emergencyContactNumber: ''
  };

  //property to change button's style
  isEditing: boolean = false;

  // properties for validations
  @ViewChild("userForm") form: any;


  // dependency injection
  constructor(private studentSvc: StudentService,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {

    var isIdPresent = this.activeRoute.snapshot.paramMap.has("id");

    if (isIdPresent) {
      const id = this.activeRoute.snapshot.paramMap.get("id");
      this.studentSvc.viewStudent(Number(id)).subscribe(
        (data: Student) => this.newStudent = data
      )
    }
  }

  // method that will make a request to the saveStudent() method in our service
  // and will redirect the user to / students aka our home page
  savedStudent() {
    this.studentSvc.saveStudent(this.newStudent).subscribe(
      data => {
        this.router.navigateByUrl("/students");
      }
    )
  }

  deleteStudent(id: number) {
    this.studentSvc.deleteStudent(id).subscribe(
      data => {
        this.router.navigateByUrl("/students")
      }
    )
  }

  

}
