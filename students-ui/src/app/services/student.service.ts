import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Student } from "../models/student";
import { map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class StudentService{
    
    //http://localhost:8080/api/s1/students is our link

    private getUrl: string = "http://localhost:8080/api/s1/students";

    // using dependency injection, bring in the HttpClient
    constructor(private httpClient: HttpClient) { }

    // method that will make a request to the GetMapping method in our backend
    getStudents(): Observable<Student[]> {
        return this.httpClient.get<Student[]>(this.getUrl).pipe(
            map(result => result)
        )
    }

    // method that will make a request to the post mapping() in our backend
    saveStudent(newStudent: Student): Observable<Student> {
        return this.httpClient.post<Student>(this.getUrl, newStudent);
    }

    // method that will make a request to the GetMapping method to 
    // view an individual student from our backend
    viewStudent(id: number): Observable<Student> {
        return this.httpClient.get<Student>(`${this.getUrl}/${id}`).pipe(
            map(result => result)
        )
    }

    // method that will make a request to the DeleteMapping method to 
    // delete an individual student from our backend
    deleteStudent(id: number): Observable<any> {
        return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'})
    }
    
    
}